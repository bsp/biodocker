

#-#-#-#-#-#-#-#-#-#-#
# Image building
#-#-#-#-#-#-#-#-#-#-#
docker build -t unigebsp/curl curl/ && docker push unigebsp/curl
docker build -t unigebsp/wget wget/ && docker push unigebsp/wget
docker build -t unigebsp/pigz pigz/ && docker push unigebsp/pigz

docker build -t unigebsp/sra-tools sra-tools/ && docker push unigebsp/sra-tools
docker build -t unigebsp/star star/ && docker push unigebsp/star
docker build -t unigebsp/fastqc fastqc/ && docker push unigebsp/fastqc
docker build -t unigebsp/jellyfish jellyfish && docker push unigebsp/jellyfish
docker build -t unigebsp/sga sga/ && docker push unigebsp/sga
docker build -t unigebsp/bwa bwa/ && docker push unigebsp/bwa
docker build -t unigebsp/minimap2 minimap2/ && docker push unigebsp/minimap2

# Alphafold
docker build -f alphafold/docker/Dockerfile -t unigebsp/alphafold:v2.1.1 alphafold/ && docker push unigebsp/alphafold:v2.1.1
#docker run -it --rm --entrypoint=bash unigebsp/alphafold:v2.1.1


#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
# Define shell aliases to the docker containers
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
alias curl='docker run --rm -v "$(pwd):/workdir" -w /workdir unigebsp/curl'
alias wget='docker run --rm -v "$(pwd):/workdir" -w /workdir unigebsp/wget'
alias pigz='docker run --rm -v "$(pwd):/workdir" -w /workdir unigebsp/pigz'

alias sra-tools='docker run --rm -v "$(pwd):/workdir" -w /workdir unigebsp/sra-tools'
alias STAR='docker run --rm -v "$(pwd):/workdir" -w /workdir unigebsp/star'
alias fastqc='docker run --rm -v "$(pwd):/workdir" -w /workdir unigebsp/fastqc'
alias jellyfish='docker run --rm -v "$(pwd):/workdir" -w /workdir unigebsp/jellyfish'
alias sga='docker run --rm -v "$(pwd):/workdir" -w /workdir unigebsp/sga'

alias bgzip='docker run --rm -v "$(pwd):/workdir" -w /workdir --entrypoint=bgzip unigebsp/bwa'
alias tabix='docker run --rm -v "$(pwd):/workdir" -w /workdir --entrypoint=tabix unigebsp/bwa'
alias htsfile='docker run --rm -v "$(pwd):/workdir" -w /workdir --entrypoint=htsfile unigebsp/bwa'
alias samtools='docker run --rm -v "$(pwd):/workdir" -w /workdir --entrypoint=samtools unigebsp/bwa'
alias bcftools='docker run --rm -v "$(pwd):/workdir" -w /workdir --entrypoint=bcftools unigebsp/bwa'
alias bwa='docker run --rm -v "$(pwd):/workdir" -w /workdir unigebsp/bwa'
alias minimap2='docker run --rm -v "$(pwd):/workdir" -w /workdir unigebsp/minimap2'




# TODO
unigebsp/cutadapt


# TODO with GUI
unigebsp/novnc
unigebsp/igv
unigebsp/seqmonk
unigebsp/cytoscape


